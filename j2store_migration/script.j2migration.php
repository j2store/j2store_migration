<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
/** ensure this file is being included by a parent file */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');

// Load FOF if not already loaded
if (!defined('F0F_INCLUDED'))
{
	$paths = array(
			(defined('JPATH_LIBRARIES') ? JPATH_LIBRARIES : JPATH_ROOT . '/libraries') . '/f0f/include.php',
			__DIR__ . '/fof/include.php',
	);

	foreach ($paths as $filePath)
	{
		if (!defined('F0F_INCLUDED') && file_exists($filePath))
		{
			@include_once $filePath;
		}
	}
}

// Pre-load the installer script class from our own copy of FOF
if (!class_exists('F0FUtilsInstallscript', false))
{
	@include_once __DIR__ . '/fof/utils/installscript/installscript.php';
}

// Pre-load the database schema installer class from our own copy of FOF
if (!class_exists('F0FDatabaseInstaller', false))
{
	@include_once __DIR__ . '/fof/database/installer.php';
}

// Pre-load the update utility class from our own copy of FOF
if (!class_exists('F0FUtilsUpdate', false))
{
	@include_once __DIR__ . '/fof/utils/update/update.php';
}

// Pre-load the cache cleaner utility class from our own copy of FOF
if (!class_exists('F0FUtilsCacheCleaner', false))
{
	@include_once __DIR__ . '/fof/utils/cache/cleaner.php';
}

class Com_J2MigrationInstallerScript extends F0FUtilsInstallscript{

	/**
	 * The component's name
	 *
	 * @var   string
	 */
	protected $componentName = 'com_j2migration';

	/**
	 * The title of the component (printed on installation and uninstallation messages)
	 *
	 * @var string
	 */
	protected $componentTitle = 'Migration tool for J2Store';


	protected $minimumJoomlaVersion = '3.3.0';

	/**
	 * The list of extra modules and plugins to install on component installation / update and remove on component
	 * uninstallation.
	 *
	 * @var   array
	 */
	/* protected $installation_queue = array(		// modules => { (folder) => { (module) => { (position), (published) } }* }*
			 'modules' => array(
			 			'admin' => array(
			 							'j2store_chart' => array('j2store-module-position-3', 1),
			 							'j2store_stats_mini' => array('j2store-module-position-1', 1),
			 							'j2store_orders' => array('j2store-module-position-4', 1),
			 							'j2store_stats' => array('j2store-module-position-5', 1)
			 			),

			 			'site' => array(
			 					'mod_j2store_currency' => array('left', 0),
			 					'mod_j2store_cart' => array('left', 0)
			 					)
			 		),
			'plugins' => array(
							'content' 		=>      array('j2store' => 1),
							'system' 		=> 		array('j2store' => 1),
							'j2store' 		=>		array(
 							 				 			'shipping_standard' 		=> 1,
							   							'payment_cash' 				=> 1,
							   							'payment_moneyorder'		=>1,
							   							'payment_banktransfer'		=>1,
							 							'payment_paypal'			=>1,
							 							'payment_paymill' 			=>1,
							 							'payment_sagepay' 			=>1,
							 							'promotion_globaldiscount' 	=> 1,
							 							'report_itemised' 	=> 1,
							 							'tool_localization_data' 	=> 1,
							 							'tool_diagnostics'			=> 1,
							 						)
						)
					);

 */
public function preflight($type, $parent) {

	if(parent::preflight($type, $parent)) {

		$app = JFactory::getApplication();
		$configuration = JFactory::getConfig();
		$db = JFactory::getDbo();
		//check of curl is present
		if(!function_exists('curl_init') || !is_callable('curl_init')) {

			$msg = "<p>cURL extension is not enabled in your PHP installation. Please contact your hosting service provider</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}
			return false;
		}

		if(!function_exists('json_encode')) {

			$msg = "<p>JSON extension is not enabled in your PHP installation. Please contact your hosting service provider</p>";

			if (version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}
			return false;
		}

		//Get installed version

		//conservative method
		$xmlfile = JPATH_ADMINISTRATOR.'/components/com_j2store/j2store.xml';
		if(JFile::exists($xmlfile)) {
			$xml = JFactory::getXML($xmlfile);
			$version=(string)$xml->version;
			// abort if the current J2Store release is older
			if( version_compare( $version, '3.0.0', 'ge' ) ) {
				$parent->getParent()->abort('You already have J2Store Version 3 installed.');
				return false;
			}
		}

		//let us check the manifest cache as well. Cannot trust joomla installer

		$query = $db->getQuery(true);
		$query->select($db->quoteName('manifest_cache'))->from($db->quoteName('#__extensions'))->where($db->quoteName('element').' = '.$db->quote('com_j2store'));
		$db->setQuery($query);
		$result = $db->loadResult();

		if($result) {
			$manifest = json_decode($result);
			$version = $manifest->version;
			// abort if the current J2Store release is older
			if( version_compare( $version, '3.0.0', 'ge' ) ) {
				$parent->getParent()->abort('You already have J2Store Version 3 installed.');
				return false;
			}
		}
		
		//conservative method
		$xmlfile = JPATH_ADMINISTRATOR.'/components/com_j2store/manifest.xml';
		if(JFile::exists($xmlfile)) {
			$xml = JFactory::getXML($xmlfile);
			$version=(string)$xml->version;
			// abort if the current J2Store release is older
			if( version_compare( $version, '2.8.0', 'lt' ) ) {
				$parent->getParent()->abort('You seem to be having an old version of J2Store. You need at least 2.8.0 to migrate to Version 3.');
				return false;
			}
		}	
		return true;
	} else {
		return false;
	}
}

protected function renderPostInstallation($status, $fofInstallationStatus, $strapperInstallationStatus, $parent)
{
	$fofInstallationStatus = $this->_installFOF($parent);	
	parent::renderPostInstallation($status, $fofInstallationStatus, $strapperInstallationStatus, $parent);

}

private function _installFOF($parent)
{
	$src = $parent->getParent()->getPath('source');

	// Load dependencies
	JLoader::import('joomla.filesystem.file');
	JLoader::import('joomla.utilities.date');
	$source = $src . '/fof';

	if (!defined('JPATH_LIBRARIES'))
	{
		$target = JPATH_ROOT . '/libraries/f0f';
	}
	else
	{
		$target = JPATH_LIBRARIES . '/f0f';
	}
	$haveToInstallFOF = false;

	if (!is_dir($target))
	{
		$haveToInstallFOF = true;
	}
	else
	{
		$fofVersion = array();

		if (file_exists($target . '/version.txt'))
		{
			$rawData = JFile::read($target . '/version.txt');
			$info    = explode("\n", $rawData);
			$fofVersion['installed'] = array(
					'version'   => trim($info[0]),
					'date'      => new JDate(trim($info[1]))
			);
		}
		else
		{
			$fofVersion['installed'] = array(
					'version'   => '0.0',
					'date'      => new JDate('2011-01-01')
			);
		}

		$rawData = JFile::read($source . '/version.txt');
		$info    = explode("\n", $rawData);
		$fofVersion['package'] = array(
				'version'   => trim($info[0]),
				'date'      => new JDate(trim($info[1]))
		);

		$haveToInstallFOF = $fofVersion['package']['date']->toUNIX() > $fofVersion['installed']['date']->toUNIX();
	}

	$installedFOF = false;

	if ($haveToInstallFOF)
	{
		$versionSource = 'package';
		$installer = new JInstaller;
		$installedFOF = $installer->install($source);
	}
	else
	{
		$versionSource = 'installed';
	}

	if (!isset($fofVersion))
	{
		$fofVersion = array();

		if (file_exists($target . '/version.txt'))
		{
			$rawData = JFile::read($target . '/version.txt');
			$info    = explode("\n", $rawData);
			$fofVersion['installed'] = array(
					'version'   => trim($info[0]),
					'date'      => new JDate(trim($info[1]))
			);
		}
		else
		{
			$fofVersion['installed'] = array(
					'version'   => '0.0',
					'date'      => new JDate('2011-01-01')
			);
		}

		$rawData = JFile::read($source . '/version.txt');
		$info    = explode("\n", $rawData);
		$fofVersion['package'] = array(
				'version'   => trim($info[0]),
				'date'      => new JDate(trim($info[1]))
		);
		$versionSource = 'installed';
	}

	if (!($fofVersion[$versionSource]['date'] instanceof JDate))
	{
		$fofVersion[$versionSource]['date'] = new JDate;
	}

	return array(
			'required'  => $haveToInstallFOF,
			'installed' => $installedFOF,
			'version'   => $fofVersion[$versionSource]['version'],
			'date'      => $fofVersion[$versionSource]['date']->format('Y-m-d'),
	);
}

	private function _sqlexecute($query) {
		$db = JFactory::getDbo();
		$db->setQuery($query);
		try {
			$db->execute();
		}catch(Exception $e) {
			//do nothing as customer can do this very well by going to the tools menu
		}
	}

}


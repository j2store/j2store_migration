<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class J2MigrationViewCPanel extends F0FViewHtml
{
	
	function onDisplay($tpl = null) {

		$model = $this->getModel('Cpanels', 'J2MigrationModel');
		$db = JFactory::getDbo();
		//first run back up check
		$this->akeeba_installed = $model->is_akeeba_installed();
		$this->params = $model->getComponentParameters();
		$this->tags = $model->getTagArticles();
			
		$variable_products = array();
		$oldproducts= array();
		if($this->params->get('j2migration_backup', 0) == 1) {
			$oldproducts= $model->get_old_records('prices');
		}else {
			$tables = $db->getTableList();
			//get prefix
			$prefix = $db->getPrefix();
			
			if(in_array($prefix.'j2store_prices', $tables)){
				$query = $db->getQuery(true)->select('*')->from('#__j2store_prices');
			}else {
				$query = $db->getQuery(true)->select('*')->from('#__v2_j2store_prices');
			}
			
			try {			
				$oldproducts= $db->setQuery($query)->loadObjectList();
			}catch(Exception $e) {
				echo $e->getMessage();
			}	
		}	
		if(isset($oldproducts) && count($oldproducts)) {
			foreach($oldproducts as $oldproduct) {
				if($this->params->get('j2migration_backup', 0) == 1) {
					$quantities = $model->get_stock($oldproduct->article_id);
				}else {
					$query = $db->getQuery(true)->select('*')->from('#__j2store_productquantities');
					$quantities = $db->setQuery($query)->loadObjectList();
				}				
				if(count($quantities) > 1) {
					$variable_products[] = $oldproduct->article_id;
				}
				
			}
			$this->variable_products = $variable_products;
		}	
		
		return parent::onDisplay($tpl);
	}
	
	
	
}

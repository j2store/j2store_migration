<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */

defined('_JEXEC') or die;

   // load tooltip behavior
   JHtml::_('bootstrap.tooltip');
   JHtml::_('behavior.multiselect');
   JHtml::_('formbehavior.chosen', 'select');
   $sidebar = JHtmlSidebar::render();
   $doc = JFactory::getDocument();
   $doc->addScript(JUri::root(true).'/administrator/components/com_j2migration/assets/migration.js');
   $doc->addStyleSheet(JUri::root(true).'/administrator/components/com_j2migration/assets/migration.css');
?>

<div class="j2migration">

	<div class="information hero-unit">
		<h1><?php echo JText::_('COM_J2MIGRATION');?></h1>
		<br />
		<div class="alert alert-block alert-info">
			<p><?php echo JText::_('J2MIGRATION_GENERAL_INFORMATION');?>
		</div>
		
		<div class="alert alert-block alert-error">
			<h2><?php echo JText::_('J2MIGRATION_INSTRUCTIONS')?></h2>
			<ol>
				<li><?php echo JText::_('J2MIGRATION_DO_NOT_BROWSE_TO_ANOTHER_PAGE')?></li>
				<li><?php echo JText::_('J2MIGRATION_MIGHT_TAKE_FEW_MINUTES')?></li>				
				<li><?php echo JText::_('J2MIGRATION_DOWNLOADABLE_PRODUCT_INSTRUCTIONS')?></li>
				<li><?php echo JText::_('J2MIGRATION_REVIEW_PRODUCTS_AFTER_MIGRATION')?></li>
			</ol>
		</div>
		
	</div>
	<?php if(isset($this->variable_products)): ?>
	<div class="variable-products">
		<div class="alert alert-block alert-error">
			<strong><?php echo JText::_('J2MIGRATION_VARIABLE_PRODUCT_LIST');?></strong>
			</div>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?php echo JText::_('J2MIGRATION_ARTICLE ID'); ?>
				</tr>
			</thead>
		<tbody>
		<?php foreach ($this->variable_products as $article):?>
			<tr>
			<td>
			<?php echo $article; ?>
			</td>
			</tr>
		<?php endforeach;?>
		</tbody>
		</table>
	</div>
	
	<?php endif; ?>
	<?php if(JFactory::getConfig()->get('error_reporting') == 'none'): ?>
	<form method="post" action="index.php" name="migrationForm" id="migrationForm">
		<input type="hidden" name="option" value ="com_j2migration" />
		<input type="hidden" name="view" value="cpanels" />
		<input type="hidden" name="task" id="task" value="browse" />
	
	<div class="akeeba-backup">
		<?php if($this->akeeba_installed != 1): ?>
			<div class="alert alert-block alert-error">
				<?php echo JText::_('J2MIGRATION_ERROR_AKEEBA_BACKUP_NOT_INSTALLED'); ?>
			</div>
			
		<?php else: ?>
		<!-- Akeeba backup is installed. So make sure that the user has taken a backup. Ask a confirmation -->
			<div id="backup-done" class="alert alert-warning">
				<label for="acceptbackup" class="postsetup-main"> <input
					type="checkbox" name="acceptbackup" id="acceptbackup" value="1" />
					<?php echo JText::_('J2MIGRATION_ACCEPT_BACKUP')?>
				</label>
			</div>	
		<?php endif; ?>
		
		<?php if($this->params->get('j2migration_backup', 0) != 1): ?>
		
		<input disabled="disabled" type="button" onclick="document.getElementById('task').value='prepare'; document.migrationForm.submit();" class="btn btn-large btn-primary" id="prepare-button" name="prepare-button" value="<?php echo JText::_('J2STORE_MIGRATE_PREPARE_BUTTON_TEXT');?>" />	
			
		<?php endif; ?>
		
	</div>
	</form>	
	
	<?php if($this->params->get('j2migration_backup', 0) == 1){ ?>
		<div id="notifications" class="well" data-start-message="Migration started"></div>
		<div id="mi-spinner" style="display:none;"><img src="<?php echo JUri::root(true).'/media/jui/img/ajax-loader.gif'?>" /></div>
		
		<div id="migrate-button-block">
			<button onclick="window.j2migrate.start()"class="btn btn-large btn-primary" id="migrate-button" name="migrate-button"> <?php echo JText::_('J2STORE_MIGRATE_BUTTON_TEXT');?> </button>
		</div>
		
		<div class="reset-button pull-right">
		<form method="post" action="index.php" name="resetForm" id="resetForm">
			<input type="hidden" name="option" value ="com_j2migration" />
			<input type="hidden" name="view" value="cpanels" />
			<input type="hidden" name="task" id="task" value="resetTables" />
			<input type="button" onclick="document.getElementById('reset-button').disabled=true; document.resetForm.submit();" class="btn btn-large btn-danger" name="reset-button" id="reset-button" value="<?php echo JText::_('J2STORE_MIGRATE_RESET_BUTTON_TEXT');?>" />
			<div class="alert alert-error alert-block">
				<?php echo JText::_('J2STORE_MIGRATE_RESET_HELP'); ?>
			</div>
		</form>	
		</div>
	<?php } ?>	
			
<div class="row">
	<div class="span6">
	</div>
	<?php 
	// check if the tags already migrated
	if(count($this->tags) > 0) { ?>
	<div class="span5">
		<?php if($this->params->get('j2migration_backup', 0) == 1){ ?>
		<div class="reset-button">
		<form method="post" action="index.php" name="fixTags" id="fixTags">
			<input type="hidden" name="option" value ="com_j2migration" />
			<input type="hidden" name="view" value="cpanels" />
			<input type="hidden" name="task" id="task" value="fixTags" />
			<input type="button" onclick="document.getElementById('fix-tag-button').disabled=true; document.fixTags.submit();"
						class="btn btn-large btn-success" name="fix-tag-button" id="fix-tag-button" 
						value="<?php echo  'Migrate Tags'; //JText::_('J2STORE_MIGRATE_RESET_BUTTON_TEXT'); ?>" />
			<div class="alert alert-error alert-block">
				This will migrate the old tags {j2storecart 4} 
				<br/> to the new format {j2store} 4 | cart {/j2store}
			</div>
		</form>	
		</div>
	<?php } ?>	
	</div>
	<?php  } /*// check if tags migrated
	else { ?>
		<h4> Tags migration successfull / no tags to migrate</h4>
	<?php } */ ?>	
</div>	
	
	<?php else: ?>
		<div class="alert alert-block alert-warning">
		<?php echo JText::_('J2MIGRATION_ERROR_REPORTING_NONE'); ?>
		</div>
	<?php endif;?>
	
</div>


<script type="text/javascript">
<!--
function beginMigration() {
	migrate.start();
}

jQuery( function( $ ) {
	
});

//-->
</script>

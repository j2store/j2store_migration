/**
 * Setup (required for Joomla! 3)
 */
if(typeof(j2migration) == 'undefined') {
	var j2migration = {};
}
if(typeof(j2migration.jQuery) == 'undefined') {
	j2migration.jQuery = jQuery.noConflict();
}

(function($) {	
	$(document).on('click', '#acceptbackup', function() {
		if($('#acceptbackup').is(':checked')){			
			$('#prepare-button').attr('disabled', false);
		}else {
			$('#prepare-button').attr('disabled', true);
		}
	}); 

})(j2migration.jQuery);

(function($) {	
	var j2migrate = {
			 show_button: function( val ) {
				 if(val) {
						$('#acceptbackup').show();
					} else {
						$('#acceptbackup').hide();
					}
				 
			 },
			 
			 start_spinner: function (val) {
				 if(val) {
					 $('#mi-spinner').show();
				 } else {
					 $('#mi-spinner').hide();
				 }
			 },
			 
			 url: function(task) {
				 return 'index.php?option=com_j2migration&view=cpanels&task='+task;
			 },
			 send_request: function(values, task) {				 
				 var j2majax = $.ajax({
						url: this.url(task),
						type: 'post',
						data: values,
						dataType: 'json',
						cache: false
							
			 	 });
				 return j2majax;
			 },
			 
			 start: function() {
				 
				 //disable the button
				 this.button_control(true);
				 
				 this.start_spinner(true);
				 
				//run the first ajax request
				$('.j2notify').remove();
				 this.notify($('#notifications').data('start-message'));
				 
				 var values = {};					
				 var request = this.send_request(values, 'start');				 
				 request.done(function(json) {
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_localisation();
					 }
				 });
			 },
			 
			 migrate_localisation: function() {
				 var values = Array();
				 var request = this.send_request(values, 'localisation');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_taxes();
					 }
					 
				 });
				 
			 },
			 
			 migrate_taxes: function() {
				 var values = Array();
				 var request = this.send_request(values, 'taxes');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_products();
					 }
					 
				 });
				 
			 },
			 
			 
			 migrate_products: function() {
				 var values = Array();
				 var request = this.send_request(values, 'products');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_orders();
					 }
					 
				 });
				 
			 },
			 
			 migrate_orders: function() {
				 var values = Array();
				 var request = this.send_request(values, 'orders');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_shipping();
					 }
					 
				 });
				 
			 },
			 
			 migrate_shipping: function() {
				 var values = Array();
				 var request = this.send_request(values, 'shipping');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.migrate_complete();
					 }
					 
				 });
				 
			 },
			 
			 migrate_complete: function() {
				 var values = Array();
				 var request = this.send_request(values, 'complete');
				 request.done(function(json) {
					 
					 if(json['notification']) {
						 window.j2migrate.notify(json['notification']);
					 }
					 if(json['error']) {
						 window.j2migrate.notify(json['error']);
					 }
					 if(json['success']) {
						 //we are ready for the next step
						 window.j2migrate.notify(json['successmsg']);
						 window.j2migrate.start_spinner(false);
					 } 
				 });
				 
			 },
			 
			 notify: function(value) {
				 $('#notifications').append('<span class="j2notify">' + value + '</span>');
			 },
			 button_control: function(attr) {
				 $('#migrate-button').attr('disabled', attr);
			 }
	}
	
window.j2migrate = j2migrate;
	
})(j2migration.jQuery);



<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die;
class J2MigrationControllerCpanels extends F0FController
{
	public function execute($task)
	{

		$allowedTasks = array('browse', 'prepare', 'start', 'localisation', 'taxes', 'orders', 'products', 'shipping', 'complete', 'resetTables', 'fixTags', 'deletetables');

		if (!in_array($task, $allowedTasks)) {
			$task = 'browse';
		}
		parent::execute($task);
	}
	
	public function prepare() {
		
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		if($model->backup_old_tables()) {
			$msg =  JText::_('J2MIGRATON_NOTIFICATION_BACKUP_OLD_TABLES_COMPLETE');	
		}else {
			$msg = $model->getError();
		}
		$this->setRedirect('index.php?option=com_j2migration&view=cpanels', $msg);
	}
	
	public function resetTables() {
	
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		if($model->reset_v3_tables()) {
			$msg =  JText::_('J2MIGRATON_RESET_TABLES_COMPLETE');
		}else {
			$msg = $model->getError();
		}
		$this->setRedirect('index.php?option=com_j2migration&view=cpanels', $msg);
	}
	
	public function fixTags() {
	
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		if($model->migrateTags()) {
			$msg =  JText::_('migration of tags complete');
		}else {
			$msg = $model->getError();
		}
		$this->setRedirect('index.php?option=com_j2migration&view=cpanels', $msg);
	}
	
	
	public function start() {
		
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();		
		//let us start the migration.		
		if($model->install_new_schema()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_NEW_SCHEMA_INSTALLATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_ERROR_NEW_SCHEMA_INSTALLATION');
			$json['error'] = $model->getError();
		}
		
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
		
		echo json_encode($json);		
		$app->close();
	}
	
	public function localisation() {
		
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
	
		//first migrate options.
		if($model->migrate_localisation()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_LOCALISATION_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_LOCALISATION_MIGRATION_ERROR');
			$json['error'] = $model->getError();
		}
		
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
		
		echo json_encode($json);
		$app->close();
	}
	
	
	public function taxes() {
	
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
	
		//first migrate options.
		if($model->migrate_taxes()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_TAXES_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_TAXES_MIGRATION_ERROR');
			$json['error'] = $model->getError();
		}
	
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
	
		echo json_encode($json);
		$app->close();
	}
	
	public function products() {
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
		
		//first migrate options.
		if($model->migrate_products()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_PRODUCTS_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_PRODUCTS_MIGRATION_ERROR');
			$json['error'] = $model->getError();
		}
		
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
		
		echo json_encode($json);
		$app->close();
		
	}
	
	public function orders() {
	
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
	
		//first migrate options.
		if($model->migrate_orders()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_ORDERS_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_ORDERS_MIGRATION_ERROR');
			$json['error'] = $model->getError();
		}
	
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
	
		echo json_encode($json);
		$app->close();
	}
	
	public function shipping() {
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
	
		//first migrate options.
		if($model->migrate_shipping()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_SHIPPING_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_SHIPPING_MIGRATION_ERROR');
			$json['error'] = $model->getError();
		}
	
		if(!isset($json['error'])) {
			$json['success'] = 1;
		}
	
		echo json_encode($json);
		$app->close();
	
	}
	
	public function complete() {
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$json = array();
		
		if($model->complete_migration()) {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_MIGRATION_COMPLETE');
		}else {
			$json['notification'] = JText::_('J2MIGRATON_NOTIFICATION_MIGRATION_ERROR_COMPLETION');
			$json['error'] = $model->getError();
		}
	
		if(!isset($json['error'])) {
			$json['success'] = 1;
			$json['successmsg'] = JText::_('J2MIGRATON_NOTIFICATION_MIGRATION_SUCCESSFULLY_COMPLETED');
		}
	
		echo json_encode($json);
		$app->close();
	
	}
/*
	public function deletetables()
	{
		$app = JFactory::getApplication();
		$model = $this->getModel('Cpanels');
		$version = $app->input->getString('version');
		$key = $app->input->getString('key');

		if(empty($key) || empty($version)) return;

		if(!in_array($version, array('v2', 'v3'))) return;

		if($key != 'fnRU98Tqyw8JGZgJ') return;

		if($version == 'v3') {
			$model->delete_v3_tables();
		}

		if($version == 'v2') {
			$model->delete_v2_tables();
		}
		$this->setRedirect('index.php?option=com_j2migration&view=cpanels');
	}
	*/
}

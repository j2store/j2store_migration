<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class J2MigrationModelCpanels extends F0FModel {


	public function is_akeeba_installed() {

		$enabled = JComponentHelper::isEnabled('com_akeeba');
		return $enabled;
	}


	public function backup_old_tables() {
		$db = JFactory::getDbo();

		$params	 = $this->getComponentParameters();
		$is_backup = $params->get('j2migration_backup', 0);
		if($is_backup !=1 ) {
			if($this->rename_old_tables()) {
				return true;
			}else {
				$this->setError($this->getError());
				return false;
			}
		}
	}

	public function rename_old_tables() {
		$oldTables = $this->get_old_tables();

		$db = JFactory::getDbo();
		//get all existing tables
		$allTables = $db->getTableList();

		foreach ($oldTables as $oldTable)
		{
			$newTable = str_replace('#__j2store_', '#__v2_j2store_', $oldTable);
			if(!in_array(str_replace('#__', $db->getPrefix(), $newTable), $allTables)) {
				
				try {
					$db->setQuery('RENAME TABLE '.$db->quoteName($oldTable).' TO '.$db->quoteName($newTable));
					$db->execute();
				} catch (Exception $e) {
					$this->setError($e->getMessage());
					//these two tables are deleted when uninstalling version 2.
					if($oldTable == '#__j2store_countries' || $oldTable == '#__j2store_zones') {
						//do nothing
					}else {
						return false;
					}	
					//probably renamed already. so do nothing
					//Sometimes user might be hitting the migration button repeatedly. Of course some of them will do. If renamed already, leave it as such.
				}
			}
		}

		//if we are here, renaming went well. So set that we have already backed up the tables.
		$params	 = $this->getComponentParameters();
		$params->set('j2migration_backup', 1);

		$data	 = $params->toString();

		$query = $db->getQuery(true)
		->update($db->qn('#__extensions'))
		->set($db->qn('params') . ' = ' . $db->q($data))
		->where($db->qn('element') . ' = ' . $db->q('com_j2migration'))
		->where($db->qn('type') . ' = ' . $db->q('component'));
		$db->setQuery($query);
		$db->execute();
	}

	public function install_new_schema() {

		$errors = array();
		//first install the primary sql
		$sql = JPATH_ADMINISTRATOR.'/components/com_j2migration/sql/install.j2store.sql';
		$db = JFactory::getDbo();

		$queries = JDatabaseDriver::splitSql(file_get_contents($sql));
		foreach ($queries as $query)
		{
			$query = trim($query);
			if ($query != '' && $query{0} != '#')
			{
				$db->setQuery($query);
				try {
					$db->execute();
				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}
			}
		}

		if(count($errors)) {
			$this->setError(implode(',', $errors));
			return false;
		}
		/* //now install the localisation data.
		if($this->install_localisation_data() == false ) {
			return false;
		} */
		//if here, all went well
		return true;
	}

	private function import_sql_file($sql) {
		if(JFile::exists($sql)) {
			$db = JFactory::getDbo();
			$queries = JDatabaseDriver::splitSql(file_get_contents($sql));
			foreach ($queries as $query)
			{
				$query = trim($query);
				if ($query != '' && $query{0} != '#')
				{
					$db->setQuery($query);
					try {
						$db->execute();
					}catch(Exception $e) {
						//do nothing as customer can do this very well by going to the tools menu
						$this->setError($e->getMessage());
						return false;
					}
				}
			}
		}
		return true;
	}

	public function migrate_localisation() {

		//countries
		//delete the countries and zones tables.
		$db = JFactory::getDbo();
		$db->dropTable('countries');
		$db->dropTable('zones');

		
		/* $countries = $this->get_old_records('countries');
		if(count($countries)) {
			$data_map = array(
				'j2store_country_id'=>'country_id',
				'country_name'=>'country_name',
				'country_isocode_2'=>'country_isocode_2',
				'country_isocode_3'=>'country_isocode_3',
				'enabled'=>'state'
			);

			if($this->copy_table('countries', 'countries', $data_map) == false) return false;
		}else {
		 	$sql = JPATH_ADMINISTRATOR.'/components/com_j2migration/sql/countries.sql';
			if($this->import_sql_file($sql) == false) return false;
		}

		//zones
		$zones = $this->get_old_records('zones');
		if(count($zones)) {
			$zones_map = array(
					'j2store_zone_id'=>'zone_id',
					'country_id'=>'country_id',
					'zone_name'=>'zone_name',
					'zone_code'=>'zone_code',
					'enabled'=>'state'
			);

			if($this->copy_table('zones', 'zones', $zones_map) == false) return false;
		}else {
			$sql = JPATH_ADMINISTRATOR.'/components/com_j2migration/sql/zones.sql';
			if($this->import_sql_file($sql) == false) return false;
		} */

		//weights
		$weights = $this->get_old_records('weights');
		if(count($weights)) {
			$weights_map = array(
					'j2store_weight_id'=>'weight_class_id',
					'weight_title'=>'weight_title',
					'weight_unit'=>'weight_unit',
					'weight_value'=>'weight_value',
					'enabled'=>'state'
			);

			if($this->copy_table('weights', 'weights', $weights_map) == false) return false;
		}else {
			$sql = JPATH_ADMINISTRATOR.'/components/com_j2migration/sql/weights.sql';
			if($this->import_sql_file($sql) == false) return false;
		}

		//lengths
		$lengths = $this->get_old_records('lengths');
		if(count($lengths)) {
			$lengths_map = array(
					'j2store_length_id'=>'length_class_id',
					'length_title'=>'length_title',
					'length_unit'=>'length_unit',
					'length_value'=>'length_value',
					'enabled'=>'state'
			);

			if($this->copy_table('lengths', 'lengths', $lengths_map) == false) return false;
		}else {
			$sql = JPATH_ADMINISTRATOR.'/components/com_j2migration/sql/lengths.sql';
			if($this->import_sql_file($sql) == false) return false;
		}

		//configuration and store profiles
		if($this->migrate_storedata() === false) return false;

		//order statuses
		if($this->migrate_orderstatus() === false) return false;

		//currency
		if($this->migrate_currency() === false) return false;


		//email templates
		$emailtemplates = array(
				'j2store_emailtemplate_id'=>'emailtemplate_id',
				'email_type'=>'email_type',
				'orderstatus_id'=>'orderstatus_id',
				'group_id'=>'group_id',
				'paymentmethod'=>'paymentmethod',
				'subject'=>'subject',
				'body'=>'body',
				'language'=>'language',
				'enabled'=>'state'
		);

		if($this->copy_table('emailtemplates', 'emailtemplates', $emailtemplates) === false) return false;

		//custom fields

			$custom_fields = 	array (
						'j2store_customfield_id' => 'field_id',
						'field_table' => 'field_table',
						'field_name' => 'field_name',
						'field_namekey' => 'field_namekey',
						'field_type' => 'field_type',
						'field_value' => 'field_value',
						'enabled' => 'published',
						'ordering' => 'ordering',
						'field_options' => 'field_options',
						'field_core' => 'field_core',
						'field_required' => 'field_required',
						'field_default' => 'field_default',
						'field_access' => 'field_access',
						'field_categories' => 'field_categories',
						'field_with_sub_categories' => 'field_with_sub_categories',
						'field_frontend' => 'field_frontend',
						'field_backend' => 'field_backend',
						'field_display' => 'field_display',
						'field_display_billing' => 'field_display_billing',
						'field_display_shipping' => 'field_display_shipping',
						'field_display_payment' => 'field_display_payment'
				);

		//TODO: add a note in the instructions that the customer has to review the display settings. Because they are now reduced to three.
		if($this->copy_table('field', 'customfields', $custom_fields) === false) return false;

		//address
		if($this->migrate_address() === false) return false;
		return true;

	}

	public function migrate_storedata() {
		$this->truncate('configurations');
		$db = $this->getDb();
		//get the store profile data
		$query = $this->getQuery()->select('*')->from($this->old_prefix().'storeprofiles')->where('state=1')->order('store_id ASC');
		$store = $db->setQuery($query)->loadObject();

		//now get the configuration
		$params = JComponentHelper::getParams('com_j2store');

		//now prepare the data
		$config = array();

		$config['store_name'] = $store->store_name;
		$config['store_address_1'] = $store->store_address_1;
		$config['store_address_2'] = $store->store_address_2;
		$config['store_city'] = $store->store_city;
		$config['store_zip'] = $store->store_zip;
		$config['country_id'] = $store->country_id;
		$config['zone_id'] = $store->zone_id;
		$config['config_currency'] = $store->config_currency;
		$config['config_currency_auto'] = $store->config_currency_auto;
		$config['config_length_class_id'] = $store->config_length_class_id;
		$config['config_weight_class_id'] = $store->config_weight_class_id;
		$config['store_min_sale_qty'] = $store->store_min_sale_qty;
		$config['store_max_sale_qty'] = $store->store_max_sale_qty;
		$config['store_notify_qty'] = $store->store_notify_qty;
		$config['store_billing_layout'] = '[first_name] [last_name] [phone_1] [phone_2] [company] [tax_number]\r\n		[address_1] [address_2] [city] [zip] [country_id] [zone_id]\r\n';
		$config['store_shipping_layout'] = '[first_name] [last_name] [phone_1] [phone_2] [company]\r\n		[address_1] [address_2] [city] [zip] [country_id] [zone_id]\r\n';
		$config['invoice_prefix'] = isset($store->invoice_prefix)?$store->invoice_prefix : '';

		//now get settings from params
		$config['admin_email'] = $params->get('admin_email');
		$config['addtocart_action'] = '1';
		$config['addtocart_button_class'] = 'btn btn-success';
		$config['addtocart_placement'] = 'default';

		$config['allow_guest_checkout'] = $params->get('allow_guest_checkout');
		$config['allow_registration'] =  $params->get('allow_registration');
		$config['attachmentfolderpath'] =  $params->get('attachmentfolderpath');
		$config['catalog_mode'] =  0;
		$config['cancel_order'] =  0;
		$config['config_discount_before_tax'] =  1;
		$config['config_including_tax'] = 0; // important because in old version all prices exclude tax

		$config['config_including_tax'] = 'billing';
		$config['config_tax_default_address'] = 'store';

		$config['date_format'] = $params->get('date_format');

		$config['downloadid'] = $params->get('downloadid');
		$config['enable_coupon'] = $params->get('enable_coupon');
		$config['enable_inventory'] = $params->get('enable_inventory');
		$config['enable_voucher'] = 0;
		$config['hold_stock'] = 60;

		$config['installation_complete'] = 1;
		$config['isregister'] = $params->get('is_register');
		$config['j2store_enable_css'] = $params->get('j2store_enable_css');

		$config['load_jquery'] = $params->get('load_jquery');
		$config['price_display_options'] = $params->get('price_display_options');

		$config['show_customer_note'] = $params->get('show_customer_note');

		$config['show_login_form'] = $params->get('show_login_form');
		$config['show_postpayment_orderlink'] = $params->get('show_postpayment_orderlink');

		$config['show_price_field'] = $params->get('show_price_field');
		$config['show_qty_field'] = $params->get('show_qty_field');
		$config['show_shipping_address'] = $params->get('show_shipping_address');
		$config['show_sku'] = $params->get('show_sku');
		$config['show_tax_calculator'] = $params->get('show_tax_calculator');
		$config['show_terms'] = $params->get('show_terms');
		$config['show_thumb_cart'] = $params->get('show_thumb_cart');
		$config['termsid'] = $params->get('termsid');
		$config['terms_display_type'] = $params->get('terms_display_type');

		//now save
		$query = 'REPLACE INTO #__j2store_configurations (config_meta_key,config_meta_value) VALUES ';

		$conditions = array();
		foreach($config as $key=>$value) {
			$conditions[] = '('.$db->q(strip_tags($key)).','.$db->q($value).')';
		}
		$query .= implode(',',$conditions);
		try {
			$db->setQuery($query);
			$db->execute();
		}catch (Exception $e) {
			$this->setError($e->getMessage());
			return false;
		}

		return true;

	}

	public function migrate_orderstatus() {

		$this->truncate('orderstatuses');

		$query = $this->getQuery ()->select ( '*, orderstatus_id as j2store_orderstatus_id' )->from ( $this->old_prefix () . 'orderstatuses' );
		$old_records = $this->getDb ()->setQuery ( $query )->loadObjectList ();

		// now insert the data
		if (count ( $old_records ) > 5) {
			try {

				foreach ( $old_records as $record ) {
					$record->enabled = 1;
					// check if 6th order status id is set
					if ($record->orderstatus_id == 6) {
						// seems a sixth status is set

						// first insert the cancelled order status here
						$newrecord = new stdClass ();
						$newrecord->j2store_orderstatus_id = 6;
						$newrecord->orderstatus_name = 'J2STORE_CANCELLED';
						$newrecord->orderstatus_cssclass = 'label label-warning';
						$newrecord->orderstatus_core = 1;
						$newrecord->enabled = 1;
						$this->insertObject ( $this->new_prefix () . 'orderstatuses', $newrecord );

						// Move this record to the last place
						$record->j2store_orderstatus_id = count ( $old_records ) + 1;
					}
					$this->insertObject ( $this->new_prefix () . 'orderstatuses', $record );
				}
			} catch ( Exception $e ) {
				$this->setError ( $e->getMessage () );
				return false;
			}
		}elseif(count ( $old_records ) > 0 && count ( $old_records ) <= 5) {

			foreach ( $old_records as $record ) {
				$record->enabled = 1;
				$this->insertObject ( $this->new_prefix () . 'orderstatuses', $record );
			}
				// insert the cancelled order status here
			$newrecord = new stdClass ();
			$newrecord->j2store_orderstatus_id = 6;
			$newrecord->orderstatus_name = 'J2STORE_CANCELLED';
			$newrecord->orderstatus_cssclass = 'label label-warning';
			$newrecord->orderstatus_core = 1;
			$newrecord->enabled = 1;
			$this->insertObject ( $this->new_prefix () . 'orderstatuses', $newrecord );

		}
		return true;
	}

	public function migrate_currency() {

		$new_table = 'currencies';
		$old_table = 'currency';

		$this->truncate($new_table);

		$data = array (
				'j2store_currency_id' => 'currency_id',
				'currency_title' => 'currency_title',
				'currency_code' => 'currency_code',
				'currency_position' => 'currency_position',
				'currency_symbol' => 'currency_symbol',
				'currency_num_decimals' => 'currency_num_decimals',
				'currency_decimal' => 'currency_decimal',
				'currency_thousands' => 'currency_thousands',
				'currency_value' => 'currency_value',
				'enabled' => 'state',
				'modified_on' => 'currency_modified'
		);

		$db = $this->getDb();
		$sql = 'INSERT IGNORE INTO '.$db->qn($this->new_prefix().$new_table).'(`'.implode('`,`',array_keys($data)).'`) ';
		$sql .='SELECT '.implode(',',$data).' FROM '.$db->qn($this->old_prefix().$old_table).' AS currency ';
		$sql .='WHERE currency.state = 1 ';
		$sql .='GROUP BY currency.currency_code ';

		$db->setQuery($sql);
		try {
			$db->execute();
		}catch(Exception $e) {
			$this->setError($e->getMessage());
			return false;
		}
		return true;
	}

	public function migrate_address() {
		$this->truncate('addresses');
		$db = JFactory::getDbo();
		$allfields = $this->getDbo()->getTableColumns($this->new_prefix().'addresses');

		$old_records = $this->get_old_records('address');
		if(count($old_records)) {
			//we just need to use one record to find out the custom fields
			$record = $old_records[0];
			$newfields = array();
			foreach (get_object_vars($record) as $k => $v)
			{
				if(!array_key_exists($k, $allfields) && $k != 'id') {
					$newfields[] = $k;
				}
			}
			if(count($newfields)) {
					$error = false;
					//we have new fields. So first create those fields in the new address table
					foreach($newfields as $field) {
						if(!array_key_exists($field, $allfields)) {
							$query = 'ALTER TABLE '.$this->new_prefix().'addresses ADD `'.$field.'` TEXT NULL';
							$db->setQuery($query);

							try {
								$db->execute();
							} catch (Exception $e) {
								$this->setError($e->getMessage());
								$error = true;
							}
						}
					}
				//an error occured?
				if($error === true) return false;
			}

		}

		//now the new address table is ready to accept custom fields. So migrate now
		if($this->migrate_table('address', 'addresses', 'id as j2store_address_id') === false) return false;

	}

	public function migrate_taxes() {

		//geozones
		if($this->migrate_table('geozones', 'geozones', 'geozone_id as j2store_geozone_id, state as enabled') === false) return false;

		//geozonerules
		if($this->migrate_table('geozonerules', 'geozonerules', 'geozonerule_id as j2store_geozonerule_id') === false) return false;

		//taxrates
		if($this->migrate_table('taxrates', 'taxrates', 'taxrate_id as j2store_taxrate_id, state as enabled') === false) return false;

		//taxrules
		if($this->migrate_table('taxrules', 'taxrules', 'taxrule_id as j2store_taxrule_id') === false) return false;

		//taxprofiles
		if($this->migrate_table('taxprofiles', 'taxprofiles', 'taxprofile_id as j2store_taxprofile_id, state as enabled') === false) return false;

		return true;

	}

	public function migrate_products() {

		//options
		if($this->migrate_table('options', 'options', 'option_id as j2store_option_id, state as enabled') === false) return false;

		//optionvalues
		if($this->migrate_table('optionvalues', 'optionvalues', 'optionvalue_id as j2store_optionvalue_id') === false) return false;

		//now products
		if($this->migrate_product_data() === false) return false;

		//product options
		if($this->migrate_product_options() === false) return false;
				
		//product option values
		if($this->migrate_table('product_optionvalues', 'product_optionvalues', 'product_optionvalue_id as j2store_product_optionvalue_id, product_option_id as productoption_id') === false) return false;

		return true;
	}

	public function migrate_shipping() {

		//shipping methods
		if($this->migrate_table('shippingmethods', 'shippingmethods', 'shipping_method_id as j2store_shippingmethod_id') === false) return false;

		//shipping rates
		if($this->migrate_table('shippingrates', 'shippingrates', 'shipping_rate_id as j2store_shippingrate_id') === false) return false;

		return true;
	}
	
	public function migrate_product_options() {
		
		$old_table = 'product_options';
		$new_table = 'product_options';
		$condition = 'product_option_id as j2store_productoption_id';
		
		$db = $this->getDb();
		//truncate the new table
		$this->truncate($new_table);
		
		$error = false;
		
		$old_records = $this->get_old_records($old_table, $condition);
		// now insert the data
		if (count ( $old_records )) {
				foreach ( $old_records as $record ) {
					
					//we need to get the new product id. Currently the record holds the article id
					$query = $db->getQuery(true)->select('j2store_product_id')->from($this->new_prefix().'products')
					->where('product_source_id='.$db->q($record->product_id));
					$db->setQuery($query);
					try {
						$new_product_id = $db->loadResult();
						$record->product_id = $new_product_id; 
						$this->insertObject ( $this->new_prefix () .$new_table, $record );
					}catch (Exception $e) {
						$this->setError ( $e->getMessage () );
						$error = true;
					}	
				}
		}
		
		if($error) {
			return false;
		}
		return true;
		
	}

	public function migrate_product_data() {

		//reset
		$this->truncate('products');
		$this->truncate('variants');
		$this->truncate('product_prices');
		$this->truncate('productquantities');
		$this->truncate('productimages');

		//get the prices table, which holds the product data
		$oldproducts= $this->get_old_records_cond('prices','','article_id <> 0');
		//$oldproducts= $this->get_old_records('prices');
		foreach($oldproducts as $oldproduct) {
			$this->create_product($oldproduct);
		}
		//product files
		if($this->migrate_table('productfiles', 'productfiles', 'productfile_id as j2store_productfile_id') === false) return false;
		
		$query = 'UPDATE #__j2store_productfiles INNER JOIN #__j2store_products ON #__j2store_productfiles.product_id = #__j2store_products.product_source_id
		SET #__j2store_productfiles.product_id = IF(#__j2store_products.j2store_product_id > 0, #__j2store_products.j2store_product_id, #__j2store_productfiles.product_id)';
		try {
			$this->_db->setQuery($query)->execute();
		}catch (Exception $e) {
			//do nothing.
		}
		return true;
	}


	public function create_product($oldproduct) {

		if( (int) $oldproduct->product_enabled != 1) return;
		//check for variable products
		$quantities = $this->get_stock($oldproduct->article_id);
		$product_files = $this->get_product_files($oldproduct->article_id);

		if(count($quantities) > 1) {
			$this->create_variable_product($oldproduct);
		}elseif(count($product_files) > 0) {
			$this->create_downloadable_product($oldproduct);
		}else {
			$this->create_simple_product($oldproduct);
		}

	}

	
	public function hasProductOptions($id) {
		
		$has = 0;
		if(empty($id))
			return $has;
		$query = $this->getQuery()->select('count(product_id)')->from($this->old_prefix().'product_options')
				->where('product_id='.$id);

		try {
			$has= $this->getDb()->setQuery($query)->loadResult();
			if($has > 0 )
				$has = 1;
		} catch (Exception $e) {
			//do nothing
		}
		
		return $has;
			
	}
	
	public function create_simple_product($oldproduct) {
		$product = new stdClass();
		$product->j2store_product_id = $oldproduct->price_id;
		$product->product_source = 'com_content'; //this will always be the article
		$product->product_source_id = $oldproduct->article_id;
		$product->product_type = 'simple'; //this will always be simple.
		$product->taxprofile_id = $oldproduct->item_tax;
		$product->has_options = $this->hasProductOptions($oldproduct->article_id);

		if(isset($oldproduct->product_enabled)) {
			$product->visibility = 1;
		} else {
			$product->visibility = 0;
		}
		$product->enabled = 1;

		//save the product
		$this->insertObject($this->new_prefix().'products', $product);

		//now create the variant
		$variant = new stdClass();
		$variant->product_id = $oldproduct->price_id;
		$variant->is_master = 1;
		$variant->sku = $oldproduct->item_sku;
		$variant->price = $oldproduct->item_price;
		$variant->pricing_calculator = 'standard';
		$variant->shipping = $oldproduct->item_shipping;
		$variant->length = $oldproduct->item_length;
		$variant->width = $oldproduct->item_width;
		$variant->height = $oldproduct->item_height;
		$variant->length_class_id = $oldproduct->item_length_class_id;
		$variant->weight_class_id = $oldproduct->item_weight_class_id;
		$variant->manage_stock = $oldproduct->manage_stock;

		if($oldproduct->min_sale_qty || $oldproduct->max_sale_qty) {
			$variant->quantity_restriction = 1;
			$variant->min_sale_qty = $oldproduct->min_sale_qty;
			$variant->use_store_config_min_sale_qty = $oldproduct->use_store_config_min_sale_qty;
			$variant->max_sale_qty = $oldproduct->max_sale_qty;
			$variant->use_store_config_max_sale_qty = $oldproduct->use_store_config_max_sale_qty;
		}

		$variant->notify_qty = $oldproduct->notify_qty;
		$variant->use_store_config_notify_qty = $oldproduct->use_store_config_notify_qty;


		if($oldproduct->manage_stock) {
			$variant->manage_stock = $oldproduct->manage_stock;
			$variant->availability = 1;
			$variant->allow_backorder = 0;
		}
		//save variant
		$variant_id = $this->insertObject($this->new_prefix().'variants', $variant);

		//now product prices

		//check if special price is available
		if($oldproduct->special_price > 0) {
			
			$db = JFactory::getDbo();
			$nullDate = $db->getNullDate();

			$price = new stdClass();
			$price->variant_id = $variant_id;
			$price->customer_group_id = 1;
			$price->date_from = $nullDate;
			$price->date_to = $nullDate;			
			$price->quantity_from = 0;
			$price->price = (float) $oldproduct->special_price;
			$this->insertObject($this->new_prefix().'product_prices', $price);
		}

		//prices
		$this->create_prices($oldproduct->article_id, $variant_id);

		//now migrate stock
		$this->create_stock($oldproduct->article_id, $oldproduct->price_id, $variant_id);

		//migrate images
		$this->create_images($oldproduct);

	}

	public function create_downloadable_product($oldproduct) {

		$product = new stdClass();
		$product->j2store_product_id = $oldproduct->price_id;
		$product->product_source = 'com_content'; //this will always be the article
		$product->product_source_id = $oldproduct->article_id;
		$product->product_type = 'downloadable'; //this will always be simple.
		$product->taxprofile_id = $oldproduct->item_tax;


		if(isset($oldproduct->product_enabled)) {
			$product->visibility = 1;
		} else {
			$product->visibility = 0;
		}
		$product->enabled = 1;

		//save the product
		$this->insertObject($this->new_prefix().'products', $product);

		
		//now create the variant
		$variant = new stdClass();
		$variant->product_id = $oldproduct->price_id;
		$variant->is_master = 1;
		$variant->sku = $oldproduct->item_sku;
		$variant->price = $oldproduct->item_price;
		$variant->pricing_calculator = 'standard';
		$variant->manage_stock = $oldproduct->manage_stock;

		if($oldproduct->min_sale_qty || $oldproduct->max_sale_qty) {
			$variant->quantity_restriction = 1;
			$variant->min_sale_qty = $oldproduct->min_sale_qty;
			$variant->use_store_config_min_sale_qty = $oldproduct->use_store_config_min_sale_qty;
			$variant->max_sale_qty = $oldproduct->max_sale_qty;
			$variant->use_store_config_max_sale_qty = $oldproduct->use_store_config_max_sale_qty;
		}

		$variant->notify_qty = $oldproduct->notify_qty;
		$variant->use_store_config_notify_qty = $oldproduct->use_store_config_notify_qty;


		if($oldproduct->manage_stock) {
			$variant->manage_stock = $oldproduct->manage_stock;
			$variant->availability = 1;
			$variant->allow_backorder = 0;
		}
		//save variant
		$variant_id = $this->insertObject($this->new_prefix().'variants', $variant);

		//now product prices

		//check if special price is available
		if($oldproduct->special_price > 0) {
			$db = JFactory::getDbo();
			$nullDate = $db->getNullDate();
			
			$price = new stdClass();
			$price->variant_id = $variant_id;
			$price->customer_group_id = 1;
			$price->date_from = $nullDate;
			$price->date_to = $nullDate;
			$price->quantity_from = 0;
			$price->price = (float) $oldproduct->special_price;
			$this->insertObject($this->new_prefix().'product_prices', $price);
		}

		//prices
		$this->create_prices($oldproduct->article_id, $variant_id);

		//now migrate stock
		$this->create_stock($oldproduct->article_id, $oldproduct->price_id, $variant_id);

		//migrate images
		$this->create_images($oldproduct);

		

		
		//we should now re-set the product id to the new structure.
		/* $product_files = $this->get_new_records('productfiles');
		foreach($product_files as $file) {
			$article_id = $file->product_id;
			$product_id = $this->get_new_product_id($article_id);
			
			$newfile = new stdClass();
			$newfile = $file;
			$newfile->product_id = $product_id;
			JFactory::getDbo()->updateObject($this->new_prefix().'productfiles', $newfile, 'j2store_productfile_id');
		} */
		
	}

	public function create_variable_product($oldproduct) {

		//we can only set the basic info. Variants had to be generated manually.

		$product = new stdClass();
		$product->j2store_product_id = $oldproduct->price_id;
		$product->product_source = 'com_content'; //this will always be the article
		$product->product_source_id = $oldproduct->article_id;
		$product->product_type = 'variable'; //this will always be simple.
		$product->taxprofile_id = $oldproduct->item_tax;

		$product->visibility = 0;

		//save the product
		$this->insertObject($this->new_prefix().'products', $product);

		//now create the master variant
		$variant = new stdClass();
		$variant->product_id = $oldproduct->price_id;
		$variant->is_master = 1;
		//save master variant
		$this->insertObject($this->new_prefix().'variants', $variant);

		$this->create_images($oldproduct);


	}

	public function create_prices($product_id, $variant_id) {

		//tiered prices
		$query = $this->getQuery()->select('*')->from($this->old_prefix().'productprices')->where('product_id='.$product_id);
		$tprices = $this->getDb()->setQuery($query)->loadObjectList();
		
		$db = JFactory::getDbo();
		$nullDate = $db->getNullDate();

		if(count($tprices)) {
			foreach($tprices as $tprice) {
				$price = new stdClass();
				$price->variant_id = $variant_id;
				$price->quantity_from = $tprice->quantity_start;
				$price->date_from = $nullDate;
				$price->date_to = $nullDate;
				$price->customer_group_id = 1;
				$price->price = $tprice->price;
				$this->insertObject($this->new_prefix().'product_prices', $price);
			}
		}

		//group prices
		$query = $this->getQuery()->select('*')->from($this->old_prefix().'groupprices')->where('product_id='.$product_id);
		$gprices = $this->getDb()->setQuery($query)->loadObjectList();

		if(count($gprices)) {
			foreach($gprices as $gprice) {
				$price = new stdClass();
				$price->variant_id = $variant_id;
				$price->date_from = $nullDate;
				$price->date_to = $nullDate;
				$price->quantity_from = 0;
				$price->customer_group_id = $gprice->customer_group_id;
				$price->price = $gprice->customer_group_price;
				$this->insertObject($this->new_prefix().'product_prices', $price);
			}
		}

	}

	public function create_stock($article_id, $oldproduct_id, $variant_id) {

		$quantities = $this->get_stock($article_id);
		if(count($quantities) > 1) {
			//we have variants
			//the customer has to re-create this.


		} elseif(count($quantities) == 1) {
			if(isset($quantities[0])) {
				$stock = new stdClass();
				$stock->variant_id = $variant_id;
				$stock->quantity = $quantities[0]->quantity;
				$this->insertObject($this->new_prefix().'productquantities', $stock);
			}

		}

	}

	public function get_stock($product_id) {
		$query = $this->getQuery()->select('*')->from($this->old_prefix().'productquantities')->where('product_id='.$product_id);
		return $this->getDb()->setQuery($query)->loadObjectList();
	}

	public function get_product_files($product_id) {
		$query = $this->getQuery()->select('*')->from($this->old_prefix().'productfiles')->where('product_id='.$product_id);
		return $this->getDb()->setQuery($query)->loadObjectList();
	}

	public function create_images($oldproduct) {
		//now save the images
		if(isset($oldproduct->main_image) && !empty($oldproduct->main_image)) {
			$image = new stdClass();
			$image->product_id = $oldproduct->price_id;
			$image->main_image = $oldproduct->main_image;
			$image->thumb_image = isset($oldproduct->listview_thumb)? $oldproduct->listview_thumb : '';
			$image->additional_images = isset($oldproduct->additional_image)? $oldproduct->additional_image: '';

			//save the product images
			$this->insertObject($this->new_prefix().'productimages', $image);
		}
	}

	public function migrate_orders() {

		//coupons
		$coupons = array(
				'j2store_coupon_id'=>'coupon_id',
				'coupon_name'=>'coupon_name',
				'coupon_code'=>'coupon_code',
				'value'=>'value',
				'value_type'=>'value_type',
				'max_value'=>'max_value',
				'max_uses'=>'max_uses',
				'logged'=>'logged',
				'max_customer_uses'=>'max_customer_uses',
				'valid_from'=>'valid_from',
				'valid_to'=>'valid_to',
				'product_category'=>'product_category',
				'products'=>'products',
				'min_subtotal'=>'min_subtotal',
				'free_shipping'=>'free_shipping',
				'enabled'=>'state'
		);

		if($this->copy_table('coupons', 'coupons', $coupons) === false) return false;

		$orders = array (
				'j2store_order_id' => 'id',
				'order_id' => 'order_id',
				'invoice_prefix' => 'invoice_prefix',
				'invoice_number' => 'invoice_number',
				'token' => 'token',
				'user_id' => 'user_id',
				'user_email' => 'user_email',
				'order_total' => 'order_total',
				'order_subtotal' => 'order_subtotal',
				'order_tax' => 'order_tax',
				'order_shipping' => 'order_shipping',
				'order_shipping_tax' => 'order_shipping_tax',
				'order_discount' => 'order_discount',
				'order_surcharge' => 'order_surcharge',
				'orderpayment_type' => 'orderpayment_type',
				'transaction_id' => 'transaction_id',
				'transaction_status' => 'transaction_status',
				'transaction_details' => 'transaction_details',
				'currency_id' => 'currency_id',
				'currency_code' => 'currency_code',
				'currency_value' => 'currency_value',
				'ip_address' => 'ip_address',
				'is_shippable' => 'is_shippable',
				'is_including_tax' => '0',
				'customer_note' => 'customer_note',
				'customer_language' => 'customer_language',
				'customer_group' => 'customer_group',
				'order_state_id' => 'order_state_id',
				'order_state' => 'order_state',
				'created_on' => 'created_date'
		);

		//orders
		if($this->copy_table('orders', 'orders', $orders) === false) return false;

		//orderitems
		$orderitems = array (
				'j2store_orderitem_id' => 'orderitem_id',
				'order_id' => 'order_id',
				'product_id' => 'product_id',
				'orderitem_sku' => 'orderitem_sku',
				'orderitem_name' => 'orderitem_name',
				'orderitem_attributes' => 'orderitem_attributes',
				'orderitem_quantity' => 'orderitem_quantity',
				'orderitem_tax' => 'orderitem_tax',
				'orderitem_discount' => 'orderitem_discount',
				'orderitem_price' => 'orderitem_price',
				'orderitem_option_price' => 'orderitem_attributes_price',
				'orderitem_finalprice' => 'orderitem_final_price',
				'orderitem_finalprice_without_tax' => 'orderitem_final_price',
				'created_on' => 'modified_date'
		);
		if($this->copy_table('orderitems', 'orderitems', $orderitems) === false) return false;

		//we also need to set some data to default values. Example, product type
		$this->migrate_orderitems();

		$orderitemattributes = array (
				'j2store_orderitemattribute_id' => 'orderitemattribute_id',
				'orderitem_id' => 'orderitem_id',
				'productattributeoption_id' => 'productattributeoption_id',
				'productattributeoptionvalue_id' => 'productattributeoptionvalue_id',
				'orderitemattribute_name' => 'orderitemattribute_name',
				'orderitemattribute_value' => 'orderitemattribute_value',
				'orderitemattribute_prefix' => 'orderitemattribute_prefix',
				'orderitemattribute_price' => 'orderitemattribute_price',
				'orderitemattribute_code' => 'orderitemattribute_code',
				'orderitemattribute_type' => 'orderitemattribute_type'
		);
		//orderitem attributes
		if($this->copy_table('orderitemattributes', 'orderitemattributes', $orderitemattributes) === false) return false;

		//orderinfo
		$orderinfo = array(
				'j2store_orderinfo_id' => 'orderinfo_id',
				'order_id' => 'order_id',
				'billing_company' => 'billing_company',
				'billing_last_name' => 'billing_last_name',
				'billing_first_name' => 'billing_first_name',
				'billing_middle_name' => 'billing_middle_name',
				'billing_phone_1' => 'billing_phone_1',
				'billing_phone_2' => 'billing_phone_2',
				'billing_fax' => 'billing_fax',
				'billing_address_1' => 'billing_address_1',
				'billing_address_2' => 'billing_address_2' ,
				'billing_city' => 'billing_city',
				'billing_zone_name' => 'billing_zone_name',
				'billing_country_name' => 'billing_country_name',
				'billing_zone_id' => 'billing_zone_id',
				'billing_country_id' => 'billing_country_id',
				'billing_zip' => 'billing_zip',
				'billing_tax_number' => 'billing_tax_number',
				'shipping_company' => 'shipping_company',
				'shipping_last_name' => 'shipping_last_name',
				'shipping_first_name' => 'shipping_first_name',
				'shipping_middle_name' => 'shipping_middle_name' ,
				'shipping_phone_1' => 'shipping_phone_1',
				'shipping_phone_2' => 'shipping_phone_2',
				'shipping_fax' => 'shipping_fax',
				'shipping_address_1' => 'shipping_address_1',
				'shipping_address_2' => 'shipping_address_2',
				'shipping_city' => 'shipping_city',
				'shipping_zip' => 'shipping_zip',
				'shipping_zone_name' => 'shipping_zone_name',
				'shipping_country_name' => 'shipping_country_name',
				'shipping_zone_id' => 'shipping_zone_id',
				'shipping_country_id' => 'shipping_country_id',
				'shipping_tax_number' => 'shipping_tax_number',
				'all_billing' => 'all_billing',
				'all_shipping' => 'all_shipping',
				'all_payment' => 'all_payment'
		);

		if($this->copy_table('orderinfo', 'orderinfos', $orderinfo) === false) return false;

		//ordershipping
		if($this->migrate_table('ordershippings', 'ordershippings', 'ordershipping_id as j2store_ordershipping_id') === false) return false;

		//ordertax
		if($this->migrate_table('ordertax', 'ordertaxes', 'ordertax_id as j2store_ordertax_id') === false) return false;

		//ordercoupons
		if($this->migrate_table('order_coupons', 'ordercoupons', 'order_coupon_id as j2store_ordercoupon_id') === false) return false;
		//orderfiles
		if($this->migrate_orderdownloads() === false) return false;

		return true;

	}

	public function migrate_orderitems() {
		$query = $this->getQuery()->select ( '*' );
		$query->from ( $this->new_prefix().'orderitems');
		$new_records = $this->getDb ()->setQuery ( $query )->loadObjectList ();
		if(count($new_records)) {
			foreach ($new_records as $record) {

				/* $item->orderitem_price = $item->orderitem_price + floatval( $item->orderitem_attributes_price );
				$taxtotal = 0;
				if($show_tax)
				{
					$taxtotal = ($item->orderitem_tax / $item->orderitem_quantity);
				}
				$item->orderitem_price = $item->orderitem_price + $taxtotal;
				$item->orderitem_final_price = $item->orderitem_price * $item->orderitem_quantity;
				$order->order_subtotal += ($taxtotal * $item->orderitem_quantity); */

				$record->orderitem_price = $record->orderitem_price + floatval( $record->orderitem_option_price);

				$record->orderitem_finalprice_without_tax = $record->orderitem_finalprice = $record->orderitem_price * $record->orderitem_quantity;

				$taxtotal = ($record->orderitem_tax / $record->orderitem_quantity);

				$record->orderitem_finalprice_with_tax =  ($record->orderitem_price + $taxtotal) * $record->orderitem_quantity;

				//set some data
				$record->product_type = 'simple';

				//right now, we only have the article id as the product id. But here we need the price_id
				$pid = $this->get_old_product_id($record->product_id);
				$record->product_id = ($pid > 0) ? $pid : '';
				//find the variant_id based on the product id
				$record->variant_id = $this->get_variant_id($record);
				$this->getDb()->updateObject($this->new_prefix().'orderitems', $record, 'j2store_orderitem_id');
			}
		}
	}

	//TODO: This function needs through checks
	public function migrate_orderdownloads() {
		//this is a pretty messy job. But we can achieve it.
		$this->truncate('orderdownloads');
		$old_records = $this->get_old_records('orderfiles');
		if(count($old_records)) {

			foreach($old_records as $record) {
				//get order id based on the order item id
				$new_record = new stdClass();
				$new_record->j2store_orderdownload_id = $record->orderfile_id;
				$new_record->order_id = $this->get_order_id_from_orderitem($record->orderitem_id);
				$new_record->product_id = $this->get_product_id_from_productfile($record->productfile_id);
				$new_record->limit_count = $record->limit_count;
				$new_record->user_id = $record->user_id;
				if(empty($record->user_email)) {
					$record->user_email = (string) JFactory::getUser($record->user_id)->email;
				}
				$new_record->user_email = $record->user_email;
				$this->insertObject($this->new_prefix().'orderdownloads', $new_record);
			}
		}
	}

	public function get_order_id_from_orderitem($orderitem_id) {
		$query = $this->getQuery()->select('order_id')->from($this->old_prefix().'orderitems')->where('orderitem_id='.$orderitem_id);
		try {
			return $this->getDb()->setQuery($query)->loadResult();
		}catch(Exception $e) {
			return 0;
		}
	}

	public function get_product_id_from_productfile($productfile_id) {
		$query = $this->getQuery()->select('product_id')->from($this->old_prefix().'productfiles')->where('productfile_id='.$productfile_id);
		try {
			//this is basically the article id.
			$article_id = $this->getDb()->setQuery($query)->loadResult();
			//we now need the j2store product id
			$product_id = $this->get_new_product_id($article_id);
			return $product_id;
		}catch(Exception $e) {
			return 0;
		}
	}

	public function migrate_table($old_table, $new_table, $condition) {


		$db = $this->getDb();
		$this->truncate($new_table);
		//first drop existing new table

		$old_records = $this->get_old_records($old_table, $condition);
		// now insert the data
		if (count ( $old_records )) {
			try {
				foreach ( $old_records as $record ) {
					$this->insertObject ( $this->new_prefix () .$new_table, $record );
				}
			} catch ( Exception $e ) {
				$this->setError ( $e->getMessage () );
			}
		}
		return true;
	}

	public function copy_table($old_table, $new_table, $data) {

		$this->truncate($new_table);

		$db = $this->getDb();
		$sql = 'INSERT IGNORE INTO '.$db->qn($this->new_prefix().$new_table).'(`'.implode('`,`',array_keys($data)).'`) ';
		$sql .='SELECT '.implode(',',$data).' FROM '.$db->qn($this->old_prefix().$old_table).' AS source ';
		$db->setQuery($sql);
		try {
			$db->execute();
		}catch(Exception $e) {
			$this->setError($e->getMessage());
			return false;
		}
		return true;
	}


	public function get_old_records($old_table, $condition='') {
		$query = $this->getQuery()->select ( '*' );
		if(!empty($condition)) {
			$query->select($condition);
		}
		$query->from ( $this->old_prefix () .$old_table);
		try {
			$old_records = $this->getDb ()->setQuery ( $query )->loadObjectList ();
		} catch(Exception $e) {
			$old_records = array();
		}

		return $old_records;
	}

	public function get_old_records_cond($old_table, $condition='', $where='') {
		$query = $this->getQuery()->select ( '*' );
		if(!empty($condition)) {
			$query->select($condition);
		}
		if(!empty($where)){
			$query->where($where);
		}
		$query->from ( $this->old_prefix () .$old_table);

		$old_records = $this->getDb ()->setQuery ( $query )->loadObjectList ();

		return $old_records;
	}
	
	public function get_new_records($new_table, $condition='') {
		$query = $this->getQuery()->select ( '*' );
		if(!empty($condition)) {
			$query->select($condition);
		}
		$query->from ( $this->new_prefix().$new_table);
		try {
			$new_records = $this->getDb ()->setQuery ( $query )->loadObjectList ();
		} catch(Exception $e) {
			$new_records = array();
		}
	
		return $new_records;
	}


	public function get_all_tables() {
		$db = JFactory::getDbo();
		$tables = $db->getTableList();
		return $tables;
	}

	public function get_old_tables() {

		$oldTables = array(
				'#__j2store_address',
				'#__j2store_countries',
				'#__j2store_coupons',
				'#__j2store_currency',
				'#__j2store_emailtemplates',
				'#__j2store_field',
				'#__j2store_geozonerules',
				'#__j2store_geozones',
				'#__j2store_groupprices',
				'#__j2store_layouts',
				'#__j2store_lengths',
				'#__j2store_options',
				'#__j2store_optionvalues',
				'#__j2store_orderfiles',
				'#__j2store_orderinfo',
				'#__j2store_orderitemattributes',
				'#__j2store_orderitems',
				'#__j2store_orders',
				'#__j2store_ordershippings',
				'#__j2store_orderstatuses',
				'#__j2store_ordertax',
				'#__j2store_order_coupons',
				'#__j2store_prices',
				'#__j2store_productfiles',
				'#__j2store_productprices',
				'#__j2store_productquantities',
				'#__j2store_product_options',
				'#__j2store_product_optionvalues',
				'#__j2store_shippingmethods',
				'#__j2store_shippingrates',
				'#__j2store_storeprofiles',
				'#__j2store_taxprofiles',
				'#__j2store_taxrates',
				'#__j2store_taxrules',
				'#__j2store_weights',
				'#__j2store_zones'
		);

		return $oldTables;
	}

	public function get_backup_tables() {

		$backupTables = array(
				'#__v2_j2store_address',
				'#__v2_j2store_countries',
				'#__v2_j2store_coupons',
				'#__v2_j2store_currency',
				'#__v2_j2store_emailtemplates',
				'#__v2_j2store_field',
				'#__v2_j2store_geozonerules',
				'#__v2_j2store_geozones',
				'#__v2_j2store_groupprices',
				'#__v2_j2store_layouts',
				'#__v2_j2store_lengths',
				'#__v2_j2store_options',
				'#__v2_j2store_optionvalues',
				'#__v2_j2store_orderfiles',
				'#__v2_j2store_orderinfo',
				'#__v2_j2store_orderitemattributes',
				'#__v2_j2store_orderitems',
				'#__v2_j2store_orders',
				'#__v2_j2store_ordershippings',
				'#__v2_j2store_orderstatuses',
				'#__v2_j2store_ordertax',
				'#__v2_j2store_order_coupons',
				'#__v2_j2store_prices',
				'#__v2_j2store_productfiles',
				'#__v2_j2store_productprices',
				'#__v2_j2store_productquantities',
				'#__v2_j2store_product_options',
				'#__v2_j2store_product_optionvalues',
				'#__v2_j2store_shippingmethods',
				'#__v2_j2store_shippingrates',
				'#__v2_j2store_storeprofiles',
				'#__v2_j2store_taxprofiles',
				'#__v2_j2store_taxrates',
				'#__v2_j2store_taxrules',
				'#__v2_j2store_weights',
				'#__v2_j2store_zones'
		);

		return $backupTables;
	}


	private function getQuery() {
		return $this->getDb()->getQuery(true);
	}

	private function getDb() {
		return JFactory::getDbo();
	}

	private function old_prefix() {
		return '#__v2_j2store_';
	}

	private function new_prefix() {
		return '#__j2store_';
	}


	public function insertObject($table, &$object, $key = null)
	{
		$fields = array();
		$values = array();

		$db = JFactory::getDbo();

		$allfields = $this->getDbo()->getTableColumns($table);
		// Iterate over the object variables to build the query fields and values.
		foreach (get_object_vars($object) as $k => $v)
		{
			// Only process non-null scalars.
			if (is_array($v) or is_object($v) or $v === null)
			{
				continue;
			}

			// Ignore any internal fields.
			if ($k[0] == '_')
			{
				continue;
			}

			if(array_key_exists($k, $allfields)) {
				// Prepare and sanitize the fields and values for the database query.
				$fields[] = $db->quoteName($k);
				$values[] = $db->quote($v);
			}
		}
		// Create the base insert statement.
		$query = $db->getQuery(true)
		->insert($db->quoteName($table))
		->columns($fields)
		->values(implode(',', $values));
		// Set the query and execute the insert.
		$db->setQuery($query);

		if (!$db->execute())
		{
			return false;
		}
		$id = $db->insertid();
		return $id;
	}

	public function get_variant_id($record) {

		$variant_id = 0;
		$query = $this->getQuery()->select('j2store_variant_id')->from($this->new_prefix().'variants')
				->where('product_id='.$record->product_id)
				->where('is_master=1');
		try {
			$variant_id = $this->getDb()->setQuery($query)->loadResult();
		} catch (Exception $e) {
			//do nothing
		}
		return $variant_id;
	}
	
	public function get_new_product_id($article_id) {

		$pid= 0;
		$query = $this->getQuery()->select('j2store_product_id')->from($this->new_prefix().'products')
			->where('product_source_id='.$this->getDb()->q($article_id));

		try {
			$pid= $this->getDb()->setQuery($query)->loadResult();
		} catch (Exception $e) {
			//do nothing
		}

		return $pid;
	}
	
	public function get_old_product_id($article_id) {

		$pid= 0;
		$query = $this->getQuery()->select('price_id')->from($this->old_prefix().'prices')
		->where('article_id='.$article_id);

		try {
			$pid= $this->getDb()->setQuery($query)->loadResult();
		} catch (Exception $e) {
			//do nothing
		}

		return $pid;

	}

	public function complete_migration() {

		//migration completed. remove the menus first
		$db = JFactory::getDbo();
		$db->setQuery("DELETE FROM `#__menu` WHERE link LIKE '%com_j2store%'");
		try {
		$db->execute();
		} catch (Exception $e) {
			//do nothing.
		}

		//rename template overrides
		 $this->remove_files();

		 //now uninstall j2store 2.x

		 $query = "SELECT `extension_id` FROM #__extensions WHERE element = ".$db->Quote('com_j2store');
		 $db->setQuery($query);
		 try {
		 	$id = $db->loadResult();
		 	$installer = new JInstaller;
		 	$result = $installer->uninstall('component', $id);
		 }catch (Exception $e) {

		 }
		 
		 $query = $db->getQuery(true)->update('#__extensions')->set('enabled = 0')->where('folder = '.$db->q('j2store'));
		 $db->setQuery($query);
		 try {
		 	$db->execute();
		 }catch (Exception $e) {
		 	
		 }
		 

		 //clean cache
		 $cache = JFactory::getCache();
		 $cache->clean();

		 return true;
	}
	
		
	public function migrateTags(){
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_j2store/tables');
			/*
			 * 1. backup the content table first (to a sql file)
			 * 2. Get the articles list
			 * load each article
			 * parse the short and long desc 
			 * replace old cart tag with new format of tags
			 * << log each article parse relusts >>
			 * */
			$db = $this->getDb();
			//1. backup the content table to a sql file
			require_once(JPATH_ADMINISTRATOR.'/components/com_j2migration/helpers/backup.php');
			$bkp  = new J2MigrationBackup();
			try {
				$bkp->backup($db) ;
			} catch (Exception $e) {
				// handle error
				print_r($e);
				}
			
			// 2. get list of articles and parse
			$query = $db->getQuery(true);
			$query->select ('c.id, c.introtext, c.fulltext') //
				-> from('#__content as c') 
				-> where(' c.introtext LIKE '.$db->q("%{j2storecart%").' OR c.fulltext LIKE '.$db->q("%{j2storecart%"));
			$db->setQuery($query);	
			$articles = $db->loadObjectList();
			
			// 3. parse tags for each article
			$map = array();
			$i=0;
			foreach($articles as $art){
					//~ echo '<br/> main article id->'.$art->id;
					
					// load the article and replace each time and save each article
					try {
						$ctable = JTable::getInstance( 'Content', 'JTable' );
						$ctable->load($art->id);
					} catch(Exception $e) {  }

					// expression to search for j2storecart
					$regex		= '/{j2storecart\s+(.*?)}/i';

					// $matches[0] is full pattern match, $matches[1] is the article id
					preg_match_all($regex, $ctable->introtext, $matches, PREG_SET_ORDER);
					
					if ($matches) {
						foreach ($matches as $match) {
							//~ echo ' match text-> '.$match[0];
							//$match[0] has the text.
							//check again

							if (empty($match[1])) {
								break;
							}

							$article_id = (int) $match[1];
							
							$new_id = $this->get_new_product_id($article_id);
							$output ='{j2store}'.$new_id.'|cart{/j2store}';
							
							$map[$i]['container_article'] = $art->id;
							$map[$i]['oldtext'] = $match[0];
							$map[$i]['newtext'] = $output;
							$map[$i]['old_id'] = $article_id;
							$map[$i++]['new_id'] = $new_id;
							
							$ctable->introtext = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $ctable->introtext, 1);
						
							}
						}

					// $matches[0] is full pattern match, $matches[1] is the article id
					preg_match_all($regex, $ctable->fulltext, $matches, PREG_SET_ORDER);
					
					if ($matches) {
						foreach ($matches as $match) {

							if (empty($match[1])) {
								break;
							}

							$article_id = (int) $match[1];
							
							$new_id = $this->get_new_product_id($article_id);
							$output ='{j2store}'.$new_id.'|cart{/j2store}';
							
							$map[$i]['container_article'] = $art->id;
							$map[$i]['oldtext'] = $match[0];
							$map[$i]['newtext'] = $output;
							$map[$i]['old_id'] = $article_id;
							$map[$i++]['new_id'] = $new_id;
							
							$ctable->fulltext = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $ctable->fulltext, 1);
						
						}
					}
				// save article
				try {
					$ctable->store();
				} catch(Exception $e) { /*print_r($e); */ }
			}
		}

	public function getTagArticles(){
			$db = $this->getDb();
			//get list of articles and parse
			$query = $db->getQuery(true);
			$query->select ('c.id, c.introtext, c.fulltext') //
				-> from('#__content as c') 
				-> where(' c.introtext LIKE '.$db->q("%{j2storecart%").' OR c.fulltext LIKE '.$db->q("%{j2storecart%"));
			$db->setQuery($query);	
			$articles = $db->loadObjectList();
			
			// parse tags for each article
			$map = array();			$i=0;
			foreach($articles as $art){
					// expression to search for j2storecart
					$regex		= '/{j2storecart\s+(.*?)}/i';
					// $matches[0] is full pattern match, $matches[1] is the article id
					preg_match_all($regex, $art->introtext, $matches, PREG_SET_ORDER);
					
					if ($matches) {
						foreach ($matches as $match) {
							//~ echo ' match text-> '.$match[0];
							//$match[0] has the text.
							//check again

							if (empty($match[1])) {
								break;
							}

							$article_id = (int) $match[1];
							
							$new_id = $this->get_new_product_id($article_id);
							$output ='{j2store}'.$new_id.'|cart{/j2store}';
							
							$map[$i]['container_article'] = $art->id;
							$map[$i]['oldtext'] = $match[0];
							$map[$i]['newtext'] = $output;
							$map[$i]['old_id'] = $article_id;
							$map[$i++]['new_id'] = $new_id;
							
							$art->introtext = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $art->introtext, 1);
						}
					}
				}
			return $map;
		}

	

	public function remove_files() {
		jimport('joomla.filesystem.file');
		$db = JFactory::getDbo();

		$error = false;
		//----file removal//
		// Rename j2store products module
		if (JFolder::exists(JPATH_SITE.'/modules/mod_j2store_products'))
		{
			if (JFolder::exists(JPATH_SITE.'/modules/old_mod_j2store_products'))
			{
				if (!JFolder::delete(JPATH_SITE.'/modules/old_mod_j2store_products'))
				{
					$this->setError('Could not delete folder '.JPATH_SITE.'/modules/old_mod_j2store_products. Check permissions.');
					return false;
				}
			}
			if (!JFolder::move(JPATH_SITE.'/modules/mod_j2store_products', JPATH_SITE.'/modules/old_mod_j2store_products'))
			{
				$this->setError('Could not move folder '.JPATH_SITE.'/modules/mod_j2store_products. Check permissions.');
				return false;
			}

		}

		// Rename j2store detail cart
		if (JFolder::exists(JPATH_SITE.'/modules/mod_j2store_detailcart'))
		{
			if (JFolder::exists(JPATH_SITE.'/modules/old_mod_j2store_detailcart'))
			{
				if (!JFolder::delete(JPATH_SITE.'/modules/old_mod_j2store_detailcart'))
				{
					$this->setError('Could not delete folder '.JPATH_SITE.'/modules/old_mod_j2store_detailcart. Check permissions.');
					return false;
				}
			}
			if (!JFolder::move(JPATH_SITE.'/modules/mod_j2store_detailcart', JPATH_SITE.'/modules/old_mod_j2store_detailcart'))
			{
				$this->setError('Could not move folder '.JPATH_SITE.'/modules/mod_j2store_detailcart. Check permissions.');
				return false;
			}

		}

		//check in the template overrides.

		//first get the default template
		$query = "SELECT template FROM #__template_styles WHERE client_id = 0 AND home=1";
		$db->setQuery( $query );
		$template = $db->loadResult();

		$template_path = JPATH_SITE.'/templates/'.$template.'/html';

		//j2store overrides
		if (JFolder::exists($template_path.'/com_j2store'))
		{
			if (JFolder::exists($template_path.'/old_com_j2store'))
			{
				if (!JFolder::delete($template_path.'/old_com_j2store'))
				{
					$this->setError('Could not delete folder '.$template_path.'/com_j2store  Check permissions.');
					return false;
				}
			}
			if (!JFolder::move($template_path.'/com_j2store', $template_path.'/old_com_j2store'))
			{
				$this->setError('Could not move folder '.$template_path.'/com_j2store . Check permissions.');
				return false;
			}

		}

		//products module template override
		if (JFolder::exists($template_path.'/mod_j2store_products'))
		{
			if (JFolder::exists($template_path.'/old_mod_j2store_products'))
			{
				if (!JFolder::delete($template_path.'/old_mod_j2store_products'))
				{
					$this->setError('Could not delete folder '.$template_path.'/old_mod_j2store_products. Check permissions.');
					$error = true;
				}
			}
			if (!JFolder::move($template_path.'/mod_j2store_products', $template_path.'/old_mod_j2store_products'))
			{
				$this->setError('Could not move folder '.$template_path.'/mod_j2store_products. Check permissions.');
				$error = true;
			}

		}

		//detailcart module template override
		if (JFolder::exists($template_path.'/mod_j2store_detailcart'))
		{
			if (JFolder::exists($template_path.'/old_mod_j2store_detailcart'))
			{
				if (!JFolder::delete($template_path.'/old_mod_j2store_detailcart'))
				{
					$this->setError('Could not delete folder '.$template_path.'/old_mod_j2store_detailcart. Check permissions.');
					$error = true;
				}
			}
			if (!JFolder::move($template_path.'/mod_j2store_detailcart', $template_path.'/old_mod_j2store_detailcart'))
			{
				$this->setError('Could not move folder '.$template_path.'/mod_j2store_detailcart. Check permissions.');
				$error = true;
			}

		}


		//cart
		if (JFolder::exists($template_path.'/mod_j2store_cart'))
		{
			if (JFolder::exists($template_path.'/old_mod_j2store_cart'))
			{
				if (!JFolder::delete($template_path.'/old_mod_j2store_cart'))
				{
					$this->setError('Could not delete folder '.$template_path.'/old_mod_j2store_cart. Check permissions.');
					$error = true;
				}
			}
			if (!JFolder::move($template_path.'/mod_j2store_cart', $template_path.'/old_mod_j2store_cart'))
			{
				$this->setError('Could not move folder '.$template_path.'/mod_j2store_cart. Check permissions.');
				$error = true;
			}

		}
		//----end of file removal//

		return $error;
	}

	/**
	 * Fetches the com_j2store component's parameters as a JRegistry instance
	 *
	 * @return JRegistry The component parameters
	 */
	public function getComponentParameters()
	{
	/* 	JLoader::import('joomla.registry.registry');

		$component = JComponentHelper::getComponent('com_j2migration');

		if ($component->params instanceof JRegistry)
		{
			$cparams = $component->params;
		}
		elseif (!empty($component->params))
		{
			$cparams = new JRegistry($component->params);
		}
		else
		{
			$cparams = new JRegistry('{}');
		} */
		
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true)->select('params')->from('#__extensions')->where('element='.$db->q('com_j2migration'))->where('type='.$db->q('component'));
		$db->setQuery($query);
		try {
			$params = $db->loadResult();
		}catch (Exception $e) {
			$params = '';
		}
		//convert to array
		if(!empty($params)) {
			$aparams = new JRegistry($params);
		}else{
			$aparams = new JRegistry('{}');
		}
		
		return $aparams;
	}

	public function truncate($new_table) {
		$db = $this->getDb();
		$db->truncateTable($this->new_prefix().$new_table);
	}
	
	public function reset_v3_tables() {
		
		$tables = $this->get_v3_tables();
		foreach($tables as $table) {
			$this->getDb()->dropTable($table);
		}
		
		if($this->install_new_schema() == false) return false;
		return true;		
	}

	public function delete_v3_tables() {
		$tables = $this->get_v3_tables();
		foreach($tables as $table) {
			$this->getDb()->dropTable($table);
		}
	}

	public function delete_v2_tables() {
		$tables = $this->get_old_tables();
		foreach($tables as $table) {
			$this->getDb()->dropTable($table);
		}
	}
	
	public function get_v3_tables() {
		
		$tables = array(
				'#__j2store_addresses',
				'#__j2store_carts',
				'#__j2store_configurations',
				'#__j2store_countries',
				'#__j2store_coupons',
				'#__j2store_currencies',
				'#__j2store_customfields',
				'#__j2store_emailtemplates',
				'#__j2store_geozonerules',
				'#__j2store_geozones',
				'#__j2store_invoicetemplates',
				'#__j2store_lengths',
				'#__j2store_manufacturers',
				'#__j2store_options',
				'#__j2store_optionvalues',
				'#__j2store_ordercoupons',
				'#__j2store_orderdownloads',
				'#__j2store_orderhistories',
				'#__j2store_orderinfos',
				'#__j2store_orderitemattributes',
				'#__j2store_orderitems',
				'#__j2store_orders',
				'#__j2store_ordershippings',
				'#__j2store_orderstatuses',
				'#__j2store_ordertaxes',
				'#__j2store_product_options',
				'#__j2store_product_optionvalues',
				'#__j2store_product_prices',
				'#__j2store_product_variant_optionvalues',
				'#__j2store_productfiles',
				'#__j2store_productfilters',
				'#__j2store_productfiltervalues',
				'#__j2store_productimages',
				'#__j2store_productprice_index',
				'#__j2store_productprices',
				'#__j2store_productquantities',
				'#__j2store_products',
				'#__j2store_shippingmethods',
				'#__j2store_shippingrates',
				'#__j2store_taxprofiles',
				'#__j2store_taxrates',
				'#__j2store_taxrules',
				'#__j2store_uploads',
				'#__j2store_variants',
				'#__j2store_vendors',
				'#__j2store_voucherhistories',
				'#__j2store_vouchers',
				'#__j2store_weights',
				'#__j2store_zones',
				'#__j2store_prices'
		);
		return $tables;		
	}

}

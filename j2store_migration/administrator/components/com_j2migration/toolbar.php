<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */

defined('_JEXEC') or die;

class J2MigrationToolbar extends F0FToolbar
{
	public function j2storeHelperRenderSubmenu($vName)
	{

		return $this->renderSubmenu($vName);
	}

	public function renderSubmenu($vName = null)
	{
		$app = JFactory::getApplication();
		if(is_null($vName)) {
			$vName = $this->input->getCmd('view','cpanels');
		}

		$this->input->set('view', $vName);

    	//parent::renderSubmenu(); //render menubar tab
		$views = array(
				'cpanel',
				'COM_J2STORE_MAINMENU_CATALOG' => array(
						array('name'=>'products','icon'=>'fa fa-tags'),
						array('name'=>'options','icon'=>'fa fa-list-ol'),
						array('name'=>'productfilters','icon'=>'fa fa-list-ol'),
						array('name'=>'vendors','icon'=>'fa fa-list-ol'),
						array('name'=>'manufacturers','icon'=>'fa fa-list-ol'),

				)
		);

		foreach($views as $label => $view) {
			if(!is_array($view)) {
				$this->addSubmenuLink($view);
			} else {
				$label = JText::_($label);
				$this->appendLink($label, '', false);
				foreach($view as $v) {
					$this->addSubmenuLink($v['name'], $label, $v['icon']);
					//	$this->renderCategorySubmenu($v);

				}
			}
		}
	}

	private function addSubmenuLink($view, $parent = null, $icon=null)
	{
		static $activeView = null;
		if(empty($activeView)) {
			$activeView = $this->input->getCmd('view','cpanel');
		}

		if ($activeView == 'cpanels')
		{
			$activeView = 'cpanel';
		}

		$key = strtoupper($this->component).'_TITLE_'.strtoupper($view);
		if(strtoupper(JText::_($key)) == $key) {
			$altview = F0FInflector::isPlural($view) ? F0FInflector::singularize($view) : F0FInflector::pluralize($view);
			$key2 = strtoupper($this->component).'_TITLE_'.strtoupper($altview);
			if(strtoupper(JText::_($key2)) == $key2) {
				$name = ucfirst($view);
			} else {
				$name = JText::_($key2);
			}
		} else {
			$name = JText::_($key);
		}

		$link = 'index.php?option='.$this->component.'&view='.$view;

		$active = $view == $activeView;

		$this->appendLink($name, $link, $active, $icon, $parent);
	}

	public function onCpanelsBrowse()
	{
		$this->renderSubmenu();

		$option = $this->input->getCmd('option', 'com_foobar');
		$componentName = str_replace('com_', '', $option);
		$subtitle_key = strtoupper($option . '_TITLE_' . F0FInflector::pluralize($this->input->getCmd('view', 'cpanel')));
		JToolBarHelper::title(JText::_(strtoupper($option)) . ': ' . JText::_($subtitle_key), $componentName);
		JToolBarHelper::preferences($option, 550, 875);
	}
}
